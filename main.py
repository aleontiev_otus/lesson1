import os
import collections
import argparse
import logging

from natural_language_in_code import get_top_verbs_in_path

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('top_size', type=int, help='an integer, for a limit on the number of words')
args = parser.parse_args()


wds = []
projects = [
    'django',
    'flask',
    'pyramid',
    'reddit',
    'requests',
    'sqlalchemy',
]
for project in projects:
    path = os.path.join('.', project)
    wds += get_top_verbs_in_path(path)

top_size = args.top_size
logging.info('total %s unique words' % len(wds))

wds = collections.Counter(dict(wds)).most_common(top_size)
for word, occurence in wds:
    print(word, occurence)
