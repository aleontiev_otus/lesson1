# code_natural - Natural language code analyzer

## Describe

The library allows you to analyze the source code and select from the definitions used words of the natural language.

## Quick start

 - Install requirements, see in file requirements.txt. 
 `pip install -f requirements.txt`
 - And download data for nltk `import nltk; nltk.download()`

## Functions

### get_all_words_in_path()

**Input args:**

- `path`: Local path where files will be analyzed

**Return:** `list`

Returns a list of words used in definitions at the specified path.

#### Example

```python
from natural_language_in_code import get_all_words_in_path

words = get_all_words_in_path('.')
```

### get_top_functions_names_in_path()

**Input args:**

- `path`: Local path where files will be analyzed

**Return:** `list`

Returns a list of words used in only function names at the specified path.

#### Example

```python
from natural_language_in_code import get_top_functions_names_in_path

words = get_top_functions_names_in_path('.')
```

### get_top_verbs_in_path()

**Input args:**

- `path`: Local path where files will be analyzed

**Return:** `list`

Returns a list of verbs used in only function names at the specified path.

#### Example

```python
from natural_language_in_code import get_top_verbs_in_path

words = get_top_verbs_in_path('.')
```
