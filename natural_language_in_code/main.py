import ast
import os
import logging

from nltk import pos_tag


def __is_verb(word):
    tags = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']

    if not word:
        return False

    pos_info = pos_tag([word])
    return pos_info[0][1] in tags


def __get_all_pyfiles(path):
    filenames = []

    for dirname, dirs, files in os.walk(path, topdown=True):
        for file in files:
            if not file.endswith('.py'):
                continue

            filenames.append(os.path.join(dirname, file))
            if len(filenames) == 100:
                break

    return filenames


def __get_trees(path, with_filenames=False, with_file_content=False):
    trees = []
    filenames = __get_all_pyfiles(path)
    logging.info('total %s files' % len(filenames))

    for filename in filenames:
        with open(filename, 'r', encoding='utf-8') as attempt_handler:
            main_file_content = attempt_handler.read()

        try:
            tree = ast.parse(main_file_content)
        except SyntaxError as e:
            logging.info(e)
            continue

        row = []

        if with_filenames:
            row.append(filename)

        if with_file_content:
            row.append(main_file_content)

        row.append(tree)
        trees.append(tuple(row) if len(row) > 1 else row[0])

    logging.info('trees generated')
    return trees


def __get_defs(trees, type_node):
    names = []
    for t in trees:
        if not t:
            continue

        for node in ast.walk(t):
            if not isinstance(node, type_node):
                continue

            f = node.name.lower() if type_node != ast.Name else node.id
            if not f.startswith('__') and not f.endswith('__'):
                names.append(f)

    return names


def __get_verbs(lists):
    verbs = []
    for name in lists:
        for _word in name.split('_'):
            if __is_verb(_word):
                verbs.append(_word)

    return verbs
