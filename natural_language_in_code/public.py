import collections
import logging

from .main import __get_defs, __get_trees, __get_verbs
from .main import ast


def get_all_words_in_path(path):
    trees = __get_trees(path)
    function_names = __get_defs(trees, ast.Name)

    words = []
    for f in function_names:
        words += f.split('_')

    return words


def get_top_verbs_in_path(path, top_size=10):
    trees = __get_trees(path)
    fncs = __get_defs(trees, ast.FunctionDef)
    logging.debug('functions extracted')
    verbs = __get_verbs(fncs)

    return collections.Counter(verbs).most_common(top_size)


def get_top_functions_names_in_path(path, top_size=10):
    trees = __get_trees(path)
    fncs = __get_defs(trees, ast.FunctionDef)

    return collections.Counter(fncs).most_common(top_size)
